#include <iostream>
#include <iomanip>
#include <chrono>

void checkSort(int array[], int n);
void minHeapify(int array[], int n, int i);
void heapSort(int array[], int n);
void printArray(int array[], int n);

int main()
{
    int exampleArray[] = { 5, 66, 12, 9, 9, 99, -3, 11 };
    int exampleArraySize = sizeof(exampleArray) / sizeof(exampleArray[0]);
    std::cout << "Example array: ";
    printArray(exampleArray, exampleArraySize);
    std::cout << "Example array sorting...\n";
    auto start = std::chrono::steady_clock::now();
    heapSort(exampleArray, exampleArraySize);
    auto end = std::chrono::steady_clock::now();
    std::chrono::duration<double> diff = end - start;
    std::cout << "Time spent: " << std::fixed << std::chrono::duration <double, std::milli>(diff).count() << " ms\n";
    std::cout << "Sorted example array: \n";
    printArray(exampleArray, exampleArraySize);

    std::cout << "\nIs example array ordered by non-growth? ";
    checkSort(exampleArray, exampleArraySize);
    const int SIZE_ARRAY = 5000;
    int worstArray[SIZE_ARRAY] {};
    int bestArray[SIZE_ARRAY] {};
    int averageArray[SIZE_ARRAY] {};
    for (int i = 0; i < SIZE_ARRAY; i++)
    {
        worstArray[i] = i;
        bestArray[i] = SIZE_ARRAY - i;
        averageArray[i] = rand() % SIZE_ARRAY;
    }
    std::cout << "Is best array ordered by non-growth? ";
    checkSort(bestArray, SIZE_ARRAY);
    std::cout << "Is worst array ordered by non-growth? ";
    checkSort(worstArray, SIZE_ARRAY);
    std::cout << "Is average array ordered by non-growth? ";
    checkSort(averageArray, SIZE_ARRAY);

    std::cout << "\n---------- Testing of sorts with " << SIZE_ARRAY << " elements in each array----------\n\n";

    std::cout << "Best array sorting...\n";
    start = std::chrono::steady_clock::now();
    heapSort(bestArray, SIZE_ARRAY);
    end = std::chrono::steady_clock::now();
    diff = end - start;
    std::cout << "Is best array ordered by non-growth? ";
    checkSort(bestArray, SIZE_ARRAY);
    std::cout << "Time spent: " << std::chrono::duration <double, std::milli>(diff).count() << " ms\n\n";

    std::cout << "Worst array sorting...\n";
    start = std::chrono::steady_clock::now();
    heapSort(worstArray, SIZE_ARRAY);
    end = std::chrono::steady_clock::now();
    diff = end - start;
    std::cout << "Is worst array ordered by non-growth? ";
    checkSort(worstArray, SIZE_ARRAY);
    std::cout << "Time spent: " << std::chrono::duration <double, std::milli>(diff).count() << " ms\n\n";

    std::cout << "Average array sorting...\n";
    start = std::chrono::steady_clock::now();
    heapSort(averageArray, SIZE_ARRAY);
    end = std::chrono::steady_clock::now();
    diff = end - start;
    std::cout << "Is average array ordered by non-growth? ";
    checkSort(averageArray, SIZE_ARRAY);
    std::cout << "Time spent: " << std::chrono::duration <double, std::milli>(diff).count() << " ms\n\n";

    return 0;
}

void checkSort(int array[], int n)
{
    bool isSorted = true;
    for (int i = 0; i < n - 1; i++)
    {
        if (array[i] >= array[i + 1])
            continue;
        else
        {
            isSorted = false;
            break;
        }
    }
    isSorted ? std::cout << "YES\n" : std::cout << "NO\n";
}

void heapSort(int array[], int n)
{
    for (int i = n / 2 - 1; i >= 0; i--)
    {
        minHeapify(array, n, i);
    }
    for (int i = n - 1; i >= 0; i--)
    {
        std::swap(array[0], array[i]);
        minHeapify(array, i, 0);
    }
}

void minHeapify(int array[], int n, int i)
{
    int smallest = i;
    int left = 2 * i + 1;
    int right = 2 * i + 2;

    if (left < n && array[left] < array[smallest])
        smallest = left;

    if (right < n && array[right] < array[smallest])
        smallest = right;

    if (smallest != i)
    {
        std::swap(array[i], array[smallest]);
        minHeapify(array, n, smallest);
    }
}

void printArray(int array[], int n)
{
    for (int i = 0; i < n; ++i)
        std::cout << array[i] << " ";
    std::cout << "\n";
}
